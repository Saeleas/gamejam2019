﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void ResetGame()
    {
        GameManager.ResetGameStatic();
        SceneManager.LoadScene((int)MyScene.HALL);
    }

    public void GoToMainTitle()
    {
        GameManager.ResetGameStatic();
        SceneManager.LoadScene((int)MyScene.TITLE);
    }
}
