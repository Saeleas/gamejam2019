﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableBehaviour : MonoBehaviour
{
    public float hidingCooldownTimer = 5.0f, hideResetTimer = 10.0f;
    public Light directionalLight;
    private float _hidingCooldown;
    private bool _canHide = true, isHiding = false;
    public Animator interactableAnimator;
    private SpriteRenderer _tagSprite;
    private HeroController _heroController;
    

    private void Start()
    {
        _hidingCooldown = hidingCooldownTimer;
        _tagSprite = GetComponentInChildren<SpriteRenderer>(true);
        _heroController = FindObjectOfType<HeroController>();
    }

    private void Update()
    {
        if (isHiding && _hidingCooldown > 0.0f)
        {
            _hidingCooldown -= Time.deltaTime;
        }
        else if(_hidingCooldown <= hidingCooldownTimer)
        {
            _hidingCooldown += Time.deltaTime;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        _tagSprite.gameObject.SetActive(true);

        if (collision.CompareTag("Player"))
        {
            if (Input.GetAxis("Vertical") > 0 && _canHide) 
            {
                isHiding = true;
                directionalLight.enabled = false;
                HeroController heroController = collision.GetComponent<HeroController>();
                if (heroController)
                {
                    interactableAnimator.ResetTrigger("Exit");
                    interactableAnimator.SetTrigger("Open");
                    //if(!_heroController)
                    //    _heroController = FindObjectOfType<HeroController>();
                    //heroController.EnterState();
                    heroController.gameObject.layer = 20; // Hiding
                    if (_hidingCooldown < 0)
                    {
                        directionalLight.enabled = true;
                        isHiding = false;
                        _canHide = false;
                        StartCoroutine(hideCoroutine());
                        heroController.gameObject.layer = 13;
                        interactableAnimator.SetTrigger("Exit");
                        interactableAnimator.ResetTrigger("Open");
                    }
                }
            }
            else
            {
                interactableAnimator.ResetTrigger("Open");
                interactableAnimator.SetTrigger("Exit");
                isHiding = false;
                directionalLight.enabled = true;
                //_canHide = true;
                _hidingCooldown = hidingCooldownTimer;
                HeroController heroController = collision.GetComponent<HeroController>();
                if (heroController)
                {
                    heroController.gameObject.layer = 13; // Player
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        _tagSprite.gameObject.SetActive(false);
    }

    public IEnumerator hideCoroutine()
    {
        yield return new WaitForSeconds(hidingCooldownTimer);
        _canHide = true;
    }
}
