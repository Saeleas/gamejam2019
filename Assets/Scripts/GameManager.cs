﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum MyScene { TITLE = 0, MAIN_MENU, HALL, FIRST_ROOM, SECRET_ROOM, SCENE_LENGTH, SECOND_HOUSE, THIRD_ROOM, SECOND_SECRET_ROOM }
public enum Character { TOM = 0, JACK, LOLLY }

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public GameObject gameOverCanvas;
    internal static float time = 0;
    public static float health = 100;
    public static Character currentCharacter;

    public static Dictionary<Room, MyScene> mapRoomScene = new Dictionary<Room, MyScene>();

    public static bool hasPreviousHeroPosition = false;
    public static Vector3 previousHeroPosition;
    public static Dictionary<MyScene, Vector3> mapPreviousPosition = new Dictionary<MyScene, Vector3>();

    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);

        // avoid not recurrent things while waiting for wrong instances to be destroyed
        if (instance == this)
        {
            // create links between rooms and scenes
            mapRoomScene.Add(Room.HALL, MyScene.HALL);
            mapRoomScene.Add(Room.FIRST_ROOM, MyScene.FIRST_ROOM);
            mapRoomScene.Add(Room.SECRET_ROOM, MyScene.SECRET_ROOM);
            mapRoomScene.Add(Room.SECOND_HOUSE, MyScene.SECOND_HOUSE);
            mapRoomScene.Add(Room.THIRD_ROOM, MyScene.THIRD_ROOM);
            mapRoomScene.Add(Room.SECOND_SECRET_ROOM, MyScene.SECOND_SECRET_ROOM);
        }
    }

    public static void ResetGameStatic()
    {
        time = 0;
        health = 100;
        hasPreviousHeroPosition = false;
        mapPreviousPosition.Clear();
    }

    public void ResetGame()
    {
        gameOverCanvas.SetActive(false);
        ResetGameStatic();
    }

    public static void ChangeRoom(ChangeRoomPoint room)
    {
        //if (SceneManager.GetActiveScene().buildIndex == (int)MyScene.HALL)
        //{
        //previousHeroPosition = room.transform.GetComponentInChildren<SpawnHeroPoint>().transform.position;
        //}
        if (!mapPreviousPosition.ContainsKey((MyScene)SceneManager.GetActiveScene().buildIndex))
        {
            mapPreviousPosition.Add((MyScene)SceneManager.GetActiveScene().buildIndex, room.transform.GetComponentInChildren<SpawnHeroPoint>().transform.position);
        }
        else
        {
            mapPreviousPosition[(MyScene)SceneManager.GetActiveScene().buildIndex] = room.transform.GetComponentInChildren<SpawnHeroPoint>().transform.position;
        }
        MyScene nextScene = mapRoomScene[room.nextRoom];
        hasPreviousHeroPosition = room.nextRoom == Room.HALL;
        SceneManager.LoadScene((int)nextScene);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
    }

    public void SelectCharacterMenu()
    {
        SceneManager.LoadScene((int)MyScene.MAIN_MENU);
    }

    public void Exit()
    {
        if (Application.isEditor)
        {
            //EditorApplication.Exit(0);
        }
        else
        {
            Application.Quit();
        }
    }

    internal static void StartGame(Character hero)
    {
        currentCharacter = hero;
        ResetGameStatic();
        SceneManager.LoadScene((int)MyScene.HALL);
    }
}
