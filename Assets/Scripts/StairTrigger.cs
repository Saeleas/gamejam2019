﻿using UnityEngine;

public class StairTrigger : MonoBehaviour
{
    public StairScript stairScript;
    public static bool onStairs;

    // Start is called before the first frame update
    void Start()
    {
        if(stairScript == null)
        {
            stairScript = GetComponentInParent<StairScript>();
        }
    }

    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.CompareTag("Player") && onStairs)
    //    {
    //        stairScript.GetComponent<BoxCollider2D>().isTrigger = true;
    //        onStairs = false;
    //        // Switch to a layer that collides with everything but the player
    //        // Should switch back to the former when getting off the stairs
    //        SwitchFloorLayer(true);
    //        HeroController heroController = collision.GetComponent<HeroController>();
    //        if (heroController)
    //            heroController.climbing = false;
    //        return;
    //    }
    //}

    private void OnTriggerStay2D(Collider2D collision)
    {
        //if (collision.CompareTag("Player") && onStairs)
        //{
        //    stairScript.GetComponent<BoxCollider2D>().isTrigger = true;
        //    onStairs = false;
        //    // Switch to a layer that collides with everything but the player
        //    // Should switch back to the former when getting off the stairs
        //    SwitchFloorLayer();
        //    return;
        //}

        if (collision.CompareTag("Player"))
        {
            if (Input.GetAxis("Vertical") != 0)
            {
                //stairScript.GetComponent<BoxCollider2D>().isTrigger = false;
                SwitchStairLayer(true);
                onStairs = true;
                SwitchFloorLayer(false);
                HeroController heroController = collision.GetComponent<HeroController>();
                if (heroController)
                {
                    heroController.currentStair = stairScript;
                    heroController.climbing = true;
                }
            }
            else if (Input.GetAxis("Horizontal") != 0)
            {
                SwitchStairLayer(false);
                //stairScript.GetComponent<BoxCollider2D>().isTrigger = true;
                onStairs = false;
                // Switch to a layer that collides with everything but the player
                // Should switch back to the former when getting off the stairs
                SwitchFloorLayer(true);
                HeroController heroController = collision.GetComponent<HeroController>();
                if (heroController)
                {
                    heroController.currentStair = null;
                    heroController.climbing = false;
                }
            }
        }
    }

    private void SwitchStairLayer(bool stairCollision)
    {
        int layer = 30; // PlayerDrop layer
        if (stairCollision)
        {
            layer = 29; // Stair layer
        }

        stairScript.gameObject.layer = layer;
    }

    private void SwitchFloorLayer(bool floorCollision)
    {
        int layer = 30; // PlayerDrop layer
        if (floorCollision)
        {
            layer = 31; // Floor layer
        }

        foreach (var item in stairScript.floors)
        {
            item.layer = layer;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //if (collision.CompareTag("Player") && onStairs)
        //{
        //    stairScript.GetComponent<BoxCollider2D>().isTrigger = true;
        //    onStairs = false;
        //    return;
        //}
    }
}
