﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeRoomPoint : MonoBehaviour
{
    public Room nextRoom;
    public RoomManager roomManager;
    public bool openLeft;
    public Animator animator;
    private SpriteRenderer _tagSprite;
    private HeroController _heroController;

    private void Awake()
    {
        if (roomManager == null)
        {
            roomManager = FindObjectOfType<RoomManager>();
        }
    }

    private void Start()
    {
        _tagSprite = GetComponentInChildren<SpriteRenderer>(true);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(_tagSprite)
            _tagSprite.gameObject.SetActive(true);
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if(_tagSprite)
            _tagSprite.gameObject.SetActive(false);
    }

    public void TriggerChangeRoom()
    {
        if (animator)
        {
            animator.SetTrigger("Open");
            if(!_heroController)
                _heroController = FindObjectOfType<HeroController>();
            _heroController.EnterState();
        }
        ChangeRoom();
    }

    public void ChangeRoom()
    {
        roomManager.IssueChangeRoom(this);
    }
}
