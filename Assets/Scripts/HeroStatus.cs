﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HeroStatus : MonoBehaviour
{
    public float sanity = 100.0f;
    public float score = 0.0f;

    private void FixedUpdate()
    {
        if(sanity <= 0)
        {
            SceneManager.LoadScene("GameOver");
        }
    }
}
