﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SelectOnInput : MonoBehaviour
{

    public EventSystem eventSystem;
    public GameObject selectedObject;
    public Image speech;

    public Sprite[] speechSprites;

    private bool buttonSelected;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            eventSystem.SetSelectedGameObject(selectedObject);
            Character hero = eventSystem.currentSelectedGameObject.GetComponent<CharacterSelection>().hero;
            speech.sprite = speechSprites[(int)hero];
            speech.enabled = true;
        }

        if (Input.GetAxisRaw("Horizontal") != 0)
        {
            if (buttonSelected)
            {
                Character hero = eventSystem.currentSelectedGameObject.GetComponent<CharacterSelection>().hero;
                speech.sprite = speechSprites[(int)hero];
                speech.enabled = true;
            }
            else
            {
                buttonSelected = true;
                eventSystem.SetSelectedGameObject(selectedObject);
            }
        }
    }

    private void OnDisable()
    {
        buttonSelected = false;
    }
}
