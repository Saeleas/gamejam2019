﻿using System.Collections;
using UnityEngine;

public class EnemyTeleportBehaviour : EnemyErraticBehaviour
{
    public float coolDownTime = 5.0f;
    private FloorScript[] floors;
    private int _curFloor = 0;
    private float _coolDown;

    // Start is called before the first frame update
    void Start()
    {
        floors = FindObjectsOfType<FloorScript>();
        _coolDown = coolDownTime;
        StartCoroutine(HideCry());
    }

    private IEnumerator HideCry()
    {
        yield return new WaitForSeconds(2.0f);
        cry.SetActive(false);
        StartCoroutine(HideCry());
    }

    protected new void FixedUpdate()
    {
        base.FixedUpdate();
        float rand = Random.Range(0.0f, 1.0f);
        if (rand > 0.8f && _coolDown <= 0)
        {
            int floor = Random.Range(0, floors.Length);
            _coolDown = coolDownTime;
            if (floor == _curFloor)
            {
                return;
            }
            _curFloor = floor;
            particles.Emit(30);
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, floors[_curFloor].transform.position.y, gameObject.transform.position.z);
        }
        _coolDown -= Time.deltaTime;
    }
}
