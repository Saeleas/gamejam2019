﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawnControl : MonoBehaviour
{
    public float delayBetweenSpawns = 5;
    public GameObject[] monsterPrefabs;

    private float timer = 0;
    private MonsterSpawnPoint[] monsterSpawnGroup;
    private int spawnCount = 0;

    // Start is called before the first frame update
    void Start()
    {
        monsterSpawnGroup = FindObjectsOfType<MonsterSpawnPoint>();
        spawnCount = monsterSpawnGroup.Length;
        // if unlucky generate immediatly
        if(Random.value > 0.5)
        {
            GenerateAtRandomPoint();
        }
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer > delayBetweenSpawns)
        {
            timer = 0;
            GenerateAtRandomPoint();
        }
    }

    private void GenerateAtRandomPoint()
    {
        int chosenSpawn = Random.Range(0, spawnCount);
        int chosenMonster = Random.Range(0, monsterPrefabs.Length);
        GameObject monster = Instantiate(monsterPrefabs[chosenMonster], monsterSpawnGroup[chosenSpawn].transform.position, Quaternion.identity);
    }
}
