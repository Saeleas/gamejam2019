﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum Room { HALL = 0, FIRST_ROOM, SECRET_ROOM, SECOND_HOUSE, THIRD_ROOM, SECOND_SECRET_ROOM, ROOM_LENGTH }

public class RoomManager : MonoBehaviour
{
    public Room room;
    public HeroController heroController;
    public HeroController[] heroControllerPrefabs;
    public SpawnHeroPoint defaultSpawnPoint;

    private ChangeRoomPoint[] changeRoomPoints;

    //Awake is always called before any Start functions
    void Awake()
    {
        changeRoomPoints = FindObjectsOfType<ChangeRoomPoint>();
    }

    // Start is called before the first frame update
    void Start()
    {
        heroController = Instantiate(heroControllerPrefabs[(int)GameManager.currentCharacter]);
        if (GameManager.mapPreviousPosition.ContainsKey(GameManager.mapRoomScene[room]))
        {
            //heroController.gameObject.transform.position = GameManager.previousHeroPosition;
            heroController.gameObject.transform.position = GameManager.mapPreviousPosition[GameManager.mapRoomScene[room]];
        }
        else
        {
            if(defaultSpawnPoint != null)
            {
                heroController.gameObject.transform.position = defaultSpawnPoint.transform.position;
            }
        }
        heroController.gameObject.SetActive(true);
    }

    public void IssueChangeRoom(ChangeRoomPoint nextRoom)
    {
        StartCoroutine(ChangeRoom(nextRoom));
    }

    public IEnumerator ChangeRoom(ChangeRoomPoint nextRoom)
    {
        // wait for animation?
        yield return new WaitForSeconds(3.0f);

        // Ask GameManager to change scene
        GameManager.ChangeRoom(nextRoom);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
