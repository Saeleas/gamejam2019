﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{
    public SelectOnInput selectionHelper;

    private bool selected;

    private void Start()
    {
        if (selectionHelper == null)
            selectionHelper = FindObjectOfType<SelectOnInput>();
    }

    private void Update()
    {
        if (!selected && Input.GetKeyDown(KeyCode.Space))
        {
            selected = true;
            selectionHelper.eventSystem.currentSelectedGameObject.GetComponent<Button>().onClick.Invoke();
        }
    }

    public void LoadGame(int hero)
    {
        GameManager.StartGame((Character)hero);
    }
}
