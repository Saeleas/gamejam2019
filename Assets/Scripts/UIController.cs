﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public HeroStatus heroStatus;
    public Slider sanitySlider;

    // Update is called once per frame
    void Update()
    {
        if (heroStatus != null && sanitySlider != null)
            sanitySlider.value = heroStatus.sanity;
    }
}
