﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleManager : MonoBehaviour
{
    private bool load;

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown && !load)
        {
            load = true;
            SceneManager.LoadScene((int)MyScene.MAIN_MENU);
        }
    }
}
