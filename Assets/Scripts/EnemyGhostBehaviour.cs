﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGhostBehaviour : MonoBehaviour
{
    public float mobSpeed = 20.0f;
    public Rigidbody2D rigidBody;
    public float flipCooldownTimer = 3.0f;
    public float maxVel = 5.0f;
    private int _direction = 1, _directionY = 1;
    private float _flipCooldown;
    public ParticleSystem particles;
    public float range = 5.0f, sanityMultiplier = 0.03f;
    public CircleCollider2D influenceRange;
    public Animator anim;
    public GameObject cry;

    private void Awake()
    {
        anim.SetTrigger("Spawn");
        anim.SetInteger("direction", _direction);
    }
    // Start is called before the first frame update
    void Start()
    {
        _flipCooldown = flipCooldownTimer;
        influenceRange.radius = range;
        StartCoroutine(HideCry());
    }

    private IEnumerator HideCry()
    {
        yield return new WaitForSeconds(2.0f);
        cry.SetActive(false);
    }

    protected void FixedUpdate()
    {
        _flipCooldown -= Time.deltaTime;
        float flip = Random.Range(0.0f, 1.0f);
        float flipY = Random.Range(0.0f, 1.0f);
        if (flip > 0.8 && _flipCooldown < 0.0f)
        {
            _direction *= -1;
            _flipCooldown = Random.Range(2.0f, flipCooldownTimer);
            Vector3 curScale = transform.localScale;
            curScale.x *= -1;
            transform.localScale = curScale;
            rigidBody.velocity = Vector2.zero;
            anim.SetInteger("direction", _direction);
        }

        if (flipY > 0.8 && _flipCooldown < 0.0f)
        {
            _directionY *= -1;
            _flipCooldown = Random.Range(2.0f, flipCooldownTimer);
            Vector3 curScale = transform.localScale;
            rigidBody.velocity = Vector2.zero;
        }
        //rigidBody.MovePosition(new Vector2(transform.position.x, transform.position.y) + new Vector2(_direction * mobSpeed * Time.deltaTime, 0.0f));
        if (rigidBody.velocity.magnitude < maxVel)
            rigidBody.AddForce(new Vector2(_direction * mobSpeed, _directionY * mobSpeed), ForceMode2D.Force);
    }
}
