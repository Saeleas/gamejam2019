﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HeroController : MonoBehaviour
{
    // Variables
    public float maxSpeed = 10f; // Controlls the max speed of the character
    public float groundRadius = 0.2f; // Creates sphere with a radius of 0.2f
    public float enemyRadius = 0.2f; // Creates sphere with a radius of 0.2f
    public float sanityMultiplier = 0.1f;
    public LayerMask whatIsGround, whatIsStair, whatIsEnemy;
    public Transform groundCheck;
    public float jumpForce = 500f;
    public HeroStatus heroStatus;
    public StairScript currentStair;
    private Rigidbody2D myBody;

    bool grounded = false, onStair = false;
    Collider2D enemySighted = null;
    public bool climbing = false;
    bool facingRight = true;
    bool canJump = true;
    ChangeRoomPoint door = null;
    private bool shouldJump;
    Animator anim;

    private float direction = 1.0f;

    void Awake()
    {
        myBody = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        direction = transform.localScale.x;
    }

    internal void EnterState()
    {
        anim.SetTrigger("Enter");
    }

    private void Start()
    {
        if (SceneManager.GetActiveScene().buildIndex != (int)MyScene.SECOND_SECRET_ROOM)
            Camera.main.gameObject.transform.SetParent(transform, false);
        UIController uiController = FindObjectOfType<UIController>();
        uiController.heroStatus = heroStatus;
        uiController.heroStatus.sanity = GameManager.health;
        heroStatus.sanity = GameManager.health;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround); // Checks to see if the groundCheck is colliding with any other colliders.
        onStair = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsStair);
        //enemySighted = Physics2D.OverlapCircle(myBody.position, enemyRadius, whatIsEnemy);
        //anim.SetBool("Ground", grounded);

        //anim.SetBool("vSpeed", myBody.velocity.y);

        float moveHorizontal = Input.GetAxis("Horizontal"); // Gets input from the player.
        float moveVertical = Input.GetAxis("Vertical"); // Gets input from the player.

        //anim.SetFloat("Speed", Mathf.Abs(move));

        if (grounded)
        {
            myBody.velocity = new Vector2(moveHorizontal * maxSpeed, myBody.velocity.y); // Moves the character by the maxSpeed.
            if (moveHorizontal != 0)
                SetDirection(moveHorizontal > 0);
            else
            {
                anim.SetBool("Right", false);
                anim.SetBool("Left", false);
            }
            if (moveVertical > 0)
            {
                if (door != null)
                {
                    door.TriggerChangeRoom();
                    //anim.SetTrigger(door.openLeft ? "EnterLeft" : "EnterRight");
                }
            }
        }

        if (climbing && currentStair != null)
        {
            float actualSpeed = moveVertical * maxSpeed;
            myBody.velocity = new Vector2(actualSpeed * currentStair.direction, moveVertical <= 0 ? actualSpeed : myBody.velocity.y); // Moves the character by the maxSpeed.
            if (moveVertical != 0)
                SetDirection(actualSpeed * currentStair.direction > 0);
        }

        //if (enemySighted)
        //{
        //    ColliderDistance2D enemyDistance = enemySighted.Distance(GetComponent<CapsuleCollider2D>());
        //    //Debug.Log(enemyDistance.distance);
        //    heroStatus.sanity = Mathf.Clamp(heroStatus.sanity - sanityMultiplier * (10.0f - enemyDistance.distance), 0.0f, 100.0f);
        //}
        //else
        //{
        heroStatus.sanity = Mathf.Clamp(heroStatus.sanity + 2.0f * Time.deltaTime, -1.0f, 100.0f);
        GameManager.health = heroStatus.sanity;
        //}

        if (shouldJump)
        {
            shouldJump = false;
            myBody.AddForce(new Vector2(0, jumpForce));
        }
    }

    void Update()
    {
        if (grounded && Input.GetKeyDown(KeyCode.Space))
        {
            canJump = false;
            shouldJump = true;
            //anim.SetBool("Ground", false);
        }
    }


    void Flip() // Method to all the character to face in the opposite direction.
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale; // Gets the localScale of the character transform component and assigns it to the variable theScale
        theScale.x *= -1; // Takes the value of theScale variable and reverses it on the x axis
        transform.localScale = theScale; // Sets the localScale of the character transform and sets it to the value stored in the theScale variable.
    }

    void SetDirection(bool forward)
    {
        Vector3 theScale = transform.localScale;
        theScale.x = direction * (forward ? 1 : -1);
        transform.localScale = theScale;
        if (forward)
        {
            anim.SetBool("Right", true);
            anim.SetBool("Left", false);
        }
        else
        {
            anim.SetBool("Left", true);
            anim.SetBool("Right", false);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Stair"))
        {

        }
        else if (collision.collider.CompareTag("Platform"))
        {

        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 9 || collision.gameObject.layer == 21)
        {
            ColliderDistance2D enemyDistance = collision.Distance(GetComponent<CapsuleCollider2D>());
            EnemyErraticBehaviour enemyErraticBehaviour = collision.gameObject.GetComponentInParent<EnemyErraticBehaviour>();
            EnemyGhostBehaviour enemyGhostBehaviour = collision.gameObject.GetComponentInParent<EnemyGhostBehaviour>();
            if (enemyErraticBehaviour != null && enemyErraticBehaviour.enabled)
            {
                heroStatus.sanity = Mathf.Clamp(heroStatus.sanity - enemyErraticBehaviour.sanityMultiplier * (enemyErraticBehaviour.range - enemyDistance.distance), -1.0f, 100.0f);
                enemyErraticBehaviour.playerSighted();
                return;
            }
            if (enemyGhostBehaviour != null && enemyGhostBehaviour.enabled)
            {
                heroStatus.sanity = Mathf.Clamp(heroStatus.sanity - enemyGhostBehaviour.sanityMultiplier * (enemyGhostBehaviour.range - enemyDistance.distance), -1.0f, 100.0f);
                return;
            }
            //Debug.Log(enemyDistance.distance);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Door"))
        {
            door = collision.gameObject.GetComponent<ChangeRoomPoint>();
        }

        if (collision.gameObject.CompareTag("StairTrigger"))
        {
            myBody.velocity = new Vector2(0, 0); // Reset velocity at the edge of the stairs
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Door"))
        {
            door = null;
        }

        if (collision.gameObject.layer == 9)
        {
            EnemyErraticBehaviour enemyErraticBehaviour = collision.gameObject.GetComponentInParent<EnemyErraticBehaviour>();
            enemyErraticBehaviour.playerOutOfSight();
        }
    }
}
